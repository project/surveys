<?php

/**
 * @file
 * Manage Survey UI
 *
 */
 
/**
 * Menu callback for /node/%/manage-survey
 *
 */ 
function surveys_manage(&$form_state, $title) {
  global $user;
  $node = node_load(arg(1));

  drupal_set_title(t('Manage Questions for %title', array('%title' => check_plain($node->title))));
  
  global $user;
  $form[$node->type] = array(
    '#type' => 'fieldset',
    '#title' => t('User Guide'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => variable_get('surveys_help_'. $node->type, ''),
  );

 $form['questions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Configure your survey.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );
  
  //Get questions for this node type  
  $result = db_query("SELECT sq.qid, sq.question, sq.cid, sq.multiple, sq.enabled, sq.node_type, sc.category, sm.mid, sm.enabled, sm.comment, sm.weight
                      FROM {surveys_questions} sq 
                      LEFT JOIN {surveys_manage} sm ON sq.qid = sm.qid 
                      LEFT JOIN {surveys_categories} sc ON sq.cid = sc.cid 
                      WHERE sq.node_type = '%s' AND sm.uid = %d AND  sq.qid IN (SELECT qid FROM {surveys_manage} WHERE nid = %d AND uid = %d)
                      ORDER BY category, question ASC", 
                      $node->type, $user->uid, $node->nid, $user->uid);  
    
  $questions = array();                                                  
  // Put categories in an array
  while ($record = db_fetch_object($result)) {
    $record->nid = $node->nid;
    $questions[] = _surveys_manage($record);
  } 

  $form['questions'] += $questions;

  if ($user->uid == $node->uid) {
   $form['submit'] = array( '#type' => 'submit', '#value' => t('Save my survey'), '#disabled' => empty($questions) ? TRUE : FALSE, ); 
  }
  else {
    drupal_set_message(t('You are not the author of this content, therefore you cannot edit its survey content.'));
  }  


  return $form;
}

/**
 * Manage Form
 *
 */
function _surveys_manage($record) {
  //weight options
  for ( $i = -50; $i < 50; $i++ ) {
    $weight[$i] = $i; 
  }
  
  $form['qid'] = array(
    '#type' => 'hidden',
    '#value' => $record->qid,
  );
  $form['mid'] = array(
    '#type' => 'hidden',
    '#value' => $record->mid,
  ); 
  $form['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#return_value' => 1,
    '#default_value' => (boolean)($record->enabled),
    '#description' => t(''),
  );
      
  $form['question_display'] = array(
    '#type' => 'markup',
    '#value' => '<h3>' . $record->question . '</h3>',
  );

  $form['question'] = array(
    '#type' => 'hidden',
    '#value' => $record->question,
  );
  $form['choices'] = array(
    '#title' => t('Choices'),
    '#return_value' => 0,
    '#value' => ($record->multiple) ? t('<a href="' . base_path() . 'node/' . $record->nid . '/manage-survey/' . $record->qid . '/choices/' . $record->question . '">Edit Choices</a>') : '',
    '#return_value' => 1,
  );
  
  $form['comment'] = array(
    '#type' => 'checkbox',
    '#title' => t('Comment'),
    '#return_value' => 1,
    '#default_value' => (boolean)($record->comment),
    '#description' => t(''),
  );
  
  $form['weight'] = array(
    '#type' => 'select',
    '#title' => t('Weight'),
    '#return_value' => 1,
    '#default_value' => $record->weight,
    '#options' => $weight,
    '#description' => t(''),
  );
  $form['update'] = array(
    '#type' => ($form['mid']['#value']) ? 'checkbox' : 'hidden',
    '#title' => t('Update'),
    '#return_value' => 1,
    '#default_value' => 0,
    '#description' => t(''),
  );

  $form['remove'] = array(
    '#type' => ($form['mid']['#value']) ? 'checkbox' : 'hidden',
    '#title' => t('Remove'),
    '#return_value' => 1,
    '#default_value' => 0,
    '#description' => t(''),
  );
  return $form;
}

/**
 * Manage Submit
 *
 */
function surveys_manage_submit($form, $form_state) {
  global $user;
  
  $form_values = $form_state['values'];

  // Regardless, save the user's data, just in case they may want it later
  foreach ($form_values['questions'] as $manage) {
    if ($manage['mid'] && $manage['update']) {
      // Update an existing manage
      db_query("UPDATE {surveys_manage} SET enabled = %d, comment = %d, weight = %d WHERE mid = %d", $manage['enabled'], $manage['comment'], $manage['weight'], $manage['mid']);
      drupal_set_message(t('The question (' . $manage['question'] . ') has been updated.'));
    }
    elseif ($manage['remove'] && !$manage['update']) {
      // Delete an existing manage
      db_query("DELETE FROM {surveys_manage} WHERE mid = %d", $manage['mid']);
      drupal_set_message(t('The question (' . $manage['question'] . ') has been removed.'));
    }
    else {
      // Doesn't exist and don't use, so just ignore
    }
  }
}

/**
 * Implementation of theme_surveys_manage()
 * - View in table format
 */
function theme_surveys_manage($form) {

  $rows = array();
  $output = '';
  foreach (element_children($form['questions']) as $key) {
    $row = array();

    // Strip labels 
    $form['questions'][$key]['enabled']['#title'] = '';
    $form['questions'][$key]['question_display']['#title'] = '';
    $form['questions'][$key]['choices']['#title'] = '';
    $form['questions'][$key]['comment']['#title'] = '';
    $form['questions'][$key]['weight']['#title'] = '';
    $form['questions'][$key]['update']['#title'] = '';
    $form['questions'][$key]['remove']['#title'] = '';
        
    $row[] = drupal_render($form['questions'][$key]['enabled']);
    $row[] = drupal_render($form['questions'][$key]['question_display']);
    $row[] = drupal_render($form['questions'][$key]['choices']);
    $row[] = drupal_render($form['questions'][$key]['comment']);
    $row[] = drupal_render($form['questions'][$key]['weight']);
    $row[] = drupal_render($form['questions'][$key]['update']);
    $row[] = drupal_render($form['questions'][$key]['remove']);
    $rows[] = $row;
  }

  $header = array('Enabled', 'Questions', 'Choices', 'Enable Comment', 'Weight', 'Update / Save', 'Remove');

  // Header
  $form['questions']['#children'] = theme('table', $header, $rows);

  $output .= drupal_render($form);
  return $output;
}






