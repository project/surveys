<?php
/**
 * @file
 * User Survey UI Form
 *
 */

/**
 * Menu callback for /node/%/manage-survey/[title]
 *
 */
function surveys_form(&$form_state, $node) {

  $form[$node->type] = array(
    '#type' => 'fieldset',
    '#title' => t('User Guide'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => variable_get('surveys_guide_'. $node->type, ''),
  );
  $form['questions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Questions for ' . $node->title),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );

  // Get choices for this node
  $choices_result = db_query("SELECT cid, nid, qid, choice, enabled, weight FROM {surveys_choices} WHERE nid = %d AND enabled = 1 ORDER BY weight ASC", $node->nid);

  // Put choices in an array
  while ($choices_record = db_fetch_object($choices_result)) {
    $choices[$choices_record->qid][$choices_record->cid] = $choices_record->choice;
  }  
  
  //Get questions for this node 
  $result = db_query("SELECT sq.qid, sq.question, sq.cid, sq.multiple, sq.enabled, sq.node_type, sc.category, sm.mid, sm.enabled, sm.comment, sm.weight
                      FROM {surveys_questions} sq 
                      LEFT JOIN {surveys_manage} sm ON sq.qid = sm.qid 
                      LEFT JOIN {surveys_categories} sc ON sq.cid = sc.cid 
                      WHERE sq.node_type = '%s' AND uid = %d AND sm.enabled = 1 AND sq.qid IN (SELECT qid FROM {surveys_manage} WHERE nid = %d AND uid = %d)
                      ORDER BY category, question ASC", 
                      $node->type, $node->uid, $node->nid, $node->uid);  
    
  $questions = array();                                                  
  // Put choices on each question
  while ($record = db_fetch_object($result)) {
    $record->nid = $node->nid;
    $record->node_title = $node->title;
    $record->choices = $choices;
    $questions[] = _surveys_form($record);
  } 

  $form['questions'] += $questions;

  $form['submit'] = array( '#type' => 'submit', '#value' => t('Submit'), 
  
  );

  return $form;
}

/**
 * User Form
 *
 */
function _surveys_form($record) {
  //options
  for ($i = 1; $i < 11; $i++ ) {
    $options[$i*10] = $i; 
  }
  
  $form['qid'] = array(
    '#type' => 'hidden',
    '#value' => $record->qid,
  );
  $form['mid'] = array(
    '#type' => 'hidden',
    '#value' => $record->mid,
  ); 
  $form['nid'] = array(
    '#type' => 'hidden',
    '#value' => $record->nid,
  ); 
  $form['node_type'] = array(
    '#type' => 'hidden',
    '#value' => $record->node_type,
  );   
  $form['question_display'] = array(
    '#type' => 'markup',
    '#value' => '<h3>' . $record->question . '</h3>',
  );

  $form['question'] = array(
    '#type' => 'hidden',
    '#value' => $record->question,
  );
  if ($record->multiple && !empty($record->choices[$record->qid])) {
    $form['choices'] = array(
      '#type' => 'select',
      '#title' => t('Choices'),
      '#return_value' => 1,
      '#options' => ($record->multiple) ? $record->choices[$record->qid] : '',
    );
  }
  else {
    $form['choices'] = array(
      '#type' => 'hidden',
      '#title' => t('Choices'),
      '#return_value' => 1,
    );
  }

  $form['rating'] = array(
    '#type' => 'select',
    '#title' => t('My Rating'),
    '#return_value' => 1,
    '#default_value' => 50,
    '#options' => $options,
    '#description' => t(''),
  );

  if (SURVEYS_FIVESTAR_ENABLE) {
    $form['rating']['#type'] = 'fivestar';
    $form['rating']['#stars'] = variable_get('surveys_fivestar_stars', 5);
  }
      
  $form['comment'] = array(
    '#type' => ($record->comment) ? 'textarea' : 'hidden',
    '#title' => t('Comment'),
    '#return_value' => 1,
    '#default_value' => '',
    '#row' => 2,
    '#description' => t(''),
  );
  
  return $form;
}

/**
 * Form Submit
 *
 */
function surveys_form_submit($form, $form_state) {
  global $user;
  $votes = array();
  $form_values = $form_state['values'];

  foreach ($form_values['questions'] as $myrating) {
    if ($myrating['rid']) {
      // Update an existing ratings
      $myrating['comment'] = check_plain($myrating['comment']);
      db_query("UPDATE {surveys_ratings} SET comment = '%s' WHERE rid = %d", $myrating['comment'], $myrating['rid']);
      drupal_set_message(t('The ratings and comments on "(' . $myrating['question'] . ')" has been updated.'));
    }
    elseif (!$myrating['rid']) {
      // Create a new question
      $myrating['comment'] = check_plain($myrating['comment']);
      db_query("INSERT INTO {surveys_ratings} (nid, uid, qid, comment) VALUES (%d, %d, %d, '%s')", $myrating['nid'], $user->uid, $myrating['qid'], $myrating['comment']);
      // Use votingapi to save the ratings
      $votes[] = array('value' => $myrating['rating'], 'tag' => $myrating['question'], 'content_type' => $myrating['node_type'], 'content_id' => $myrating['nid']);
    }
    else {
      // Doesn't exist and don't use, so just ignore
    }
  }
  // votingapi_set_vote('rating', 'question', 'content_typ', 'content_id');
  votingapi_set_votes($votes);
  drupal_set_message(t('Your ratings and comments on "(' . $form_values['node_title'] . ')" has been saved.'));
}


/**
 * Implementation of theme_surveys_form()
 * - View in table format
 */
function theme_surveys_form($form) {
//dsm($form);
  $rows = array();
  $output = '';
  foreach (element_children($form['questions']) as $key) {
    $row = array();

    // Strip labels
    $form['questions'][$key]['question_display']['#title'] = '';
    $form['questions'][$key]['choices']['#title'] = '';
    $form['questions'][$key]['rating']['#title'] = '';
    $form['questions'][$key]['comment']['#title'] = '';

    $row[] = drupal_render($form['questions'][$key]['question_display']);
    $row[] = drupal_render($form['questions'][$key]['choices']);
    $row[] = drupal_render($form['questions'][$key]['rating']);
    $row[] = drupal_render($form['questions'][$key]['comment']);
    $rows[] = $row;
  }

  $header = array('Questions', 'Choices', 'My Rating', 'Comments');

  // Header
  $form['questions']['#children'] = theme('table', $header, $rows);

  $output .= drupal_render($form);
  return $output;
}






