<?php
/**
 * @file
 * Surveys Categories UI Form
 *
 */

/**
 * Menu callback for admin/settings/surveys/[node_type]/categories.
 */
function surveys_categories(&$form_state, $type, $name) {

  drupal_set_title(t('Categories for %type nodes', array('%type' => check_plain($name))));

  $form['categories'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add / Delete / Configure survey categories here.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );

  // Get categories for this node type
  $result = db_query("SELECT cid, node_type, category, enabled, description FROM {surveys_categories} WHERE node_type='%s' ORDER BY category ASC", $type);
  
  while ($record = db_fetch_object($result)) {
    $record->node_type = $type;
    $categories[] = _surveys_categories($record);
  }
  
  // add another blank category field
  $record = new stdClass();
  $record->cid = 0;
  $record->node_type = $type;
  
  $categories[] = _surveys_categories($record);

  $form['categories'] += $categories;

  $form['submit'] = array( '#type' => 'submit', '#value' => t('Save categories'),
  );
  return $form;
}

/**
 * Category Form
 *
 */
function _surveys_categories($record) {
  
  $form['cid'] = array(
    '#type' => 'hidden',
    '#value' => $record->cid,
  );
  $form['category'] = array(
    '#type' => 'textfield',
    '#title' => t('Category'),
    '#return_value' => 1,
    '#default_value' => $record->category,
    '#size' => 30,
  );
  $form['node_type'] = array(
    '#type' => 'hidden',
    '#title' => t(''),
    '#return_value' => 1,
    '#default_value' => $record->node_type,
  );  
  $form['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#return_value' => 1,
    '#default_value' => (boolean)($record->enabled),
    '#description' => t(''),
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('description'),
    '#return_value' => 1,
    '#default_value' => $record->description,
    '#rows' => 2,
  );
  $form['update'] = array(
    '#type' => ($form['cid']['#value']) ? 'checkbox' : 'hidden',
    '#title' => t('Update'),
    '#return_value' => 1,
    '#default_value' => 0,
    '#description' => t(''),
  );
  $form['delete'] = array(
    '#type' => ($form['cid']['#value']) ? 'checkbox' : 'hidden',
    '#title' => t('Delete'),
    '#return_value' => 1,
    '#default_value' => 0,
    '#description' => t(''),
  );
  return $form;
}

/**
 * Category Submit
 *
 */

function surveys_categories_submit($form, $form_state) {
  $form_values = $form_state['values'];

  // variable_set('surveys_use_' . $form_values['node_type'], $form_values['use']);
  //variable_set('surveys_guide_'. $form_values['node_type'], $form_values['guide']);

  // Regardless, save the user's data, just in case they may want it later
  foreach ($form_values['categories'] as $category) {
    if ($category['cid'] && $category['update']) {
      // Update an existing category
      $category['description'] = check_plain($category['description']);
      db_query("UPDATE {surveys_categories} SET category='%s', enabled= %d, description='%s' WHERE cid=%d", $category['category'], $category['enabled'], $category['description'], $category['cid']);
      drupal_set_message(t('The category (' . $category['category'] . ') has been updated.'));
    }
    elseif ($category['delete']) {
      // Delete an existing category
      db_query("DELETE FROM {surveys_categories} WHERE cid = %d", $category['cid']);
      drupal_set_message(t('The category (' . $category['category'] . ') has been deleted.'));
    }
    elseif (!$category['cid'] && !empty($category['category'])) {
      // Create a new category
      $category['description'] = check_plain($category['description']);
      db_query("INSERT INTO {surveys_categories} (node_type, category, enabled, description) VALUES ('%s', '%s', %d, '%s')", $category['node_type'], $category['category'], $category['enabled'], $category['description']);
      drupal_set_message(t('The category (' . $category['category'] . ') has been added.'));
    }
    else {
      // Doesn't exist and don't use, so just ignore
    }
  }
}

/**
 * Implementation of theme_hook()
 * - View in table format
 *
 */
function theme_surveys_categories($form) {
  $rows = array();
  $output = '';
  
  foreach (element_children($form['categories']) as $key) {
    $row = array();
    // Strips labes
    $form['categories'][$key]['enabled']['#title'] = '';
    $form['categories'][$key]['category']['#title'] = '';
    $form['categories'][$key]['description']['#title'] = '';

    $form['categories'][$key]['update']['#title'] = '';
    $form['categories'][$key]['delete']['#title'] = '';
        
    $row[] = drupal_render($form['categories'][$key]['enabled']);
    $row[] = drupal_render($form['categories'][$key]['category']);
    $row[] = drupal_render($form['categories'][$key]['description']);

    $row[] = drupal_render($form['categories'][$key]['update']);
    $row[] = drupal_render($form['categories'][$key]['delete']);
    $rows[] = $row;
  }

  $header = array('Enabled', 'Categories &nbsp;<i><a href="' . base_path() . 'admin/settings/surveys/' . $form['categories'][0]['node_type']['#value'] . '">Go back to Questions</a></i>', 'Description', 'Update / Save', 'Delete');

  // Header
  $form['categories']['#children'] = theme('table', $header, $rows);

  $output .= drupal_render($form);
  
  return $output;
}


