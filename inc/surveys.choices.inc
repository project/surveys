<?php
/**
 * @file
 * Choices UI
 *
 */
 
/**
 * Choices Form
 *
 */ 
function surveys_choices(&$form_state, $name) {

  drupal_set_title(t('Edit Choices for %title nodes', array('%title' => check_plain(arg(5)))));
  
  // Get questions for this node type  
  $result = db_query("SELECT sc.cid, sc.nid, sc.qid, sc.choice, sc.enabled, sc.weight, sq.qid, sq.question 
                      FROM {surveys_choices} sc JOIN {surveys_questions} sq ON sc.qid = sq.qid 
                      WHERE sc.nid = %d AND sc.qid = %d ORDER BY sc.weight ASC", 
                      arg(1), arg(3)); 
    
  $choices = array();
  // Put categories in an array
  while ($record = db_fetch_object($result)) {
    $record->nid = arg(1);
    $record->qid = arg(3);
    $choices[] = _surveys_choices($record);
  }    

  $form['choices'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add / Edit / Delete your choices.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );
  
  // add an choice
  $record = new stdClass();
  $record->nid = arg(1);
  $record->qid = arg(3);
  $record->question = arg(5);
  $choices[] = _surveys_choices($record);
  
  $form['choices'] += $choices;

  $form['submit'] = array( '#type' => 'submit', '#value' => t('Save choices'), 
  
  );

  return $form;
}

/**
 * Choices Form
 *
 */
function _surveys_choices($record) {

  //weight options
  for ( $i = -50; $i < 50; $i++ ) {
    $weight[$i] = $i; 
  }
  
  $form['cid'] = array(
    '#type' => 'hidden',
    '#value' => $record->cid,
  );
  $form['nid'] = array(
    '#type' => 'hidden',
    '#value' => $record->nid,
  );
  $form['qid'] = array(
    '#type' => 'hidden',
    '#value' => $record->qid,
  );
  $form['question'] = array(
    '#type' => 'hidden',
    '#value' => $record->question,
  );
  $form['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#return_value' => 1,
    '#default_value' => (boolean)($record->enabled),
    '#description' => t(''),
  );

  $form['choice'] = array(
    '#type' => 'textfield',
    '#title' => t('Choice'),
    '#return_value' => 1,
    '#default_value' => $record->choice,
    '#size' => 50,
  );

  $form['weight'] = array(
    '#type' => 'select',
    '#title' => t('Weight'),
    '#return_value' => 1,
    '#default_value' => $record->weight,
    '#options' => $weight,
    '#description' => t(''),
  );
        
  $form['update'] = array(
    '#type' => ($form['cid']['#value']) ? 'checkbox' : 'hidden',
    '#title' => t('Update'),
    '#return_value' => 1,
    '#default_value' => 0,
    '#description' => t(''),
  );
  $form['delete'] = array(
    '#type' => ($form['cid']['#value']) ? 'checkbox' : 'hidden',
    '#title' => t('Delete'),
    '#return_value' => 1,
    '#default_value' => 0,
    '#description' => t(''),
  );
  return $form;
}

/**
 * Choices Submit
 *
 */
function surveys_choices_submit($form, $form_state) {
  $form_values = $form_state['values'];

  foreach ($form_values['choices'] as $choice) {
    if ($choice['cid'] && $choice['update']) {
      // Update an existing choice
      db_query("UPDATE {surveys_choices} SET choice = '%s', enabled = %d, weight = %d WHERE cid = %d", $choice['choice'], $choice['enabled'], $choice['weight'], $choice['cid']);
      drupal_set_message(t('A choice for "' . $choice['question'] . '" has been updated.'));
    }
    elseif (!$choice['enabled'] && $choice['delete']) {
      // Delete an existing choice
      db_query("DELETE FROM {surveys_choices} WHERE cid = %d", $choice['cid']);
      drupal_set_message(t('A choice for "' . $choice['question'] . '" has been removed.'));
    }
    elseif (!$choice['cid'] && !empty($choice['choice'])) {
      // Create a new choice
      db_query("INSERT INTO {surveys_choices} (nid, qid, choice, enabled, weight) VALUES (%d, %d, '%s', %d, %d)", $choice['nid'], $choice['qid'],  $choice['choice'], $choice['enabled'], $choice['weight'] );
      drupal_set_message(t('A choice for "' . $choice['question'] . '" has been added.'));
    }
    else {
      // Doesn't exist and don't use, so just ignore
    }
  }
}

/**
 * Implementation of theme_surveys_choices()
 * - View in table format
 *
 */
function theme_surveys_choices($form) {
//dsm($form);
  $rows = array();
  $output = '';
  foreach (element_children($form['choices']) as $key) {
    $row = array();

    // Strip labels 
    $form['choices'][$key]['enabled']['#title'] = '';
    $form['choices'][$key]['choice']['#title'] = '';
    $form['choices'][$key]['weight']['#title'] = '';
    $form['choices'][$key]['update']['#title'] = '';
    $form['choices'][$key]['delete']['#title'] = '';
        
    $row[] = drupal_render($form['choices'][$key]['enabled']);
    $row[] = drupal_render($form['choices'][$key]['choice']);
    $row[] = drupal_render($form['choices'][$key]['weight']);
    $row[] = drupal_render($form['choices'][$key]['update']);
    $row[] = drupal_render($form['choices'][$key]['delete']);
    $rows[] = $row;
  }

  $header = array('Enabled', 'Choices', 'Weight', 'Update / Save', 'Delete');

  // Header
  $form['choices']['#children'] = theme('table', $header, $rows);

  $output .= drupal_render($form);
  return $output;
}






