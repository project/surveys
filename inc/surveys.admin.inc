<?php
/**
 * @file
 * Survey Content Types UI
 * For users with admin access
 *  - admin can assign which content type to be used in surveys
 *  - admin can add/edit/delete questions and categories
 */

/**
 * Configure Form
 *
 */ 
function surveys_configure() {

  drupal_set_title(t('Survey Types'));
  $form = array();
  $default = array();
  foreach (node_get_types() as $type => $info) {
    $options[$type] = $info->name;
    if (variable_get('surveys_use_'. $type, 0)) {
      $default[] = $type;
    }
  }
  $form['surveys'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content types'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['surveys']['types'] = array(
    '#type' => 'checkboxes',
    '#default_value' => $default,
    '#options' => $options,
    '#description' => t('Specify which content types can be surveyed.'),
  );

  if (module_exists('fivestar')) {
    $form['fivestar'] = array(
      '#type' => 'fieldset',
      '#title' => t('Fivestar rating options'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#tree' => TRUE,
    );
    $form['fivestar']['enable'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use Fivestar for surveys'),
      '#default_value' => variable_get('surveys_fivestar_enable', 0),
      '#description' => t('Enabling Fivestar will replace select list options with a JavaScript star display.'),
    );
    $form['fivestar']['stars'] = array(
      '#type' => 'select',
      '#title' => t('Number of stars'),
      '#options' => drupal_map_assoc(range(1, 10)),
      '#default_value' => variable_get('surveys_fivestar_stars', 5),
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Settings'),
  );

  return $form;
}

/**
 * Configure Submit
 *
 */
function surveys_configure_submit($form, $form_state) {
  $form_values = $form_state['values'];
  foreach ($form_values['types'] as $type => $checked) {
    variable_set('surveys_use_'. $type, (bool)$checked);
  }
  if ($form_values['fivestar']) {
    foreach ($form_values['fivestar'] as $option => $value) {
      variable_set('surveys_fivestar_'. $option, $value);
    }
  }
  cache_clear_all();
  menu_rebuild();
}

