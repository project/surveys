<?php

/**
 * @file
 * Surveys Questions UI
 *
 */

/**
 * Menu callback for /admin/settings/surveys/[node_type].
 *
 */
function surveys_questions(&$form_state, $type, $name) {

  drupal_set_title(t('Questions for %type nodes', array('%type' => check_plain($name))));

  $form[$type] = array(
      '#type' => 'fieldset',
      '#title' => t('Survey'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
  );

  $form[$type]['node_type'] = array('#type' => 'hidden', '#value' => $type);
  $form[$type]['help'] = array(
    '#type' => 'textarea',
    '#title' => t('Usage Guide to the User'),
    '#return_value' => 1,
    '#default_value' => variable_get('surveys_help_'. $type, ''),
    '#description' => t('Instructions to users for how to use this survey.  These will be shown on the "Add Survey" page. Note that if you have help text defined on admin/settings/content-types/surveys, this value will override it.'),
  );

  $form['questions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add / Delete / Configure survey questions here.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );

  // Get categories for this node type
  $cat_result = db_query("SELECT cid, node_type, category FROM {surveys_categories} WHERE node_type='%s' AND enabled = 1 ORDER BY category ASC", $type);
  
  $categories[0] = 'none';
  // Put categories in an array
  while ($cat_record = db_fetch_object($cat_result)) {
    $categories[$cat_record->cid] = $cat_record->category;
  }  
  
  // Get questions for this node type  
  $result = db_query("SELECT sq.qid, sq.question, sq.cid, sq.multiple, sq.enabled, sq.node_type, sq.description, sc.category 
                      FROM {surveys_questions} sq LEFT JOIN {surveys_categories} sc ON sq.cid = sc.cid 
                      WHERE sq.node_type = '%s' ORDER BY category, question ASC", $type);
  
  while ($record = db_fetch_object($result)) {
    $record->categories = $categories;
    $questions[] = _surveys_questions($record);
  }
  
  // add another blank question fields
  $record = new stdClass();
  $record->qid = 0;
  $record->node_type = $type;
  $record->categories = $categories;
  
  $questions[] = _surveys_questions($record);

  $form['questions'] += $questions;

  $form['submit'] = array( '#type' => 'submit', '#value' => t('Save questions'),
  
  );
  
  return $form;
}

/**
 * Questions Form
 *
 */
function _surveys_questions($record) {
  
  $form['qid'] = array(
    '#type' => 'hidden',
    '#value' => $record->qid,
  );
  $form['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#return_value' => 1,
    '#default_value' => (boolean)($record->enabled),
    '#description' => t(''),
  );
  $form['question'] = array(
    '#type' => 'textfield',
    '#title' => t('Question'),
    '#return_value' => 1,
    '#default_value' => $record->question,
    '#size' => 30,
  );
  $form['section'] = array(
    '#type' => 'hidden',
    '#title' => t(''),
    '#return_value' => 1,
    '#value' => '<tr><td colspan="7"> ' . $record->category . '</td></tr>',
  );  
  $form['node_type'] = array(
    '#type' => 'hidden',
    '#title' => t(''),
    '#return_value' => 1,
    '#default_value' => $record->node_type,
  );  
  $form['category'] = array(
    '#type' => 'select',
    '#title' => t('Category'),
    '#return_value' => 1,
    '#default_value' => $record->cid,
    '#options' => $record->categories,
  );    
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('description'),
    '#return_value' => 1,
    '#default_value' => $record->description,
    '#rows' => 2,
  );
  $form['multiple'] = array(
    '#type' => 'checkbox',
    '#title' => t('With multiple answers'),
    '#delta' => 10,
    '#default_value' => (boolean)($record->multiple),
    '#description' => t(''),
  );
  $form['update'] = array(
    '#type' => ($form['qid']['#value']) ? 'checkbox' : 'hidden',
    '#title' => t('Update'),
    '#return_value' => 1,
    '#default_value' => 0,
    '#description' => t(''),
  );
  $form['delete'] = array(
    '#type' => ($form['qid']['#value']) ? 'checkbox' : 'hidden',
    '#title' => t('Delete'),
    '#return_value' => 1,
    '#default_value' => 0,
    '#description' => t(''),
  );

  return $form;
}

/**
 * Questions Submit
 *
 */
function surveys_questions_submit($form, $form_state) {
  $form_values = $form_state['values'];

  variable_set('surveys_help_'. $form_values['node_type'], $form_values['help']);

  // Regardless, save the user's data, just in case they may want it later
  foreach ($form_values['questions'] as $question) {
    if ($question['qid'] && $question['update']) {
      // Update an existing question
      $question['description'] = check_plain($question['description']);
      db_query("UPDATE {surveys_questions} SET question='%s', cid=%d, description='%s', enabled=%d, multiple=%d WHERE qid=%d", $question['question'], $question['category'], $question['description'], $question['enabled'], $question['multiple'], $question['qid']);
      drupal_set_message(t('The question (' . $question['question'] . ') has been updated.'));
    }
    elseif (!$question['enabled'] && $question['delete']) {
      // Delete an existing question
      db_query("DELETE FROM {surveys_questions} WHERE qid=%d", $question['qid']);
      drupal_set_message(t('The question (' . $question['question'] . ') has been deleted.'));
    }
    elseif (!$question['qid'] && !empty($question['question'])) {
      // Create a new question
      $question['description'] = check_plain($question['description']);
      db_query("INSERT INTO {surveys_questions} (node_type, question, cid, description, enabled, multiple) VALUES ('%s', '%s', %d, '%s', %d, %d)", $form_values['node_type'], $question['question'], $question['category'], $question['description'], $question['enabled'], $question['multiple']);
      drupal_set_message(t('The question (' . $question['question'] . ') has been added.'));
    }
    else {
      // Doesn't exist and don't use, so just ignore
    }
  }
}


/**
 * Implementation of theme_surveys_questions()
 * - View in table format
 */
function theme_surveys_questions($form) {
//dsm($form);
  $rows = array();
  $output = '';
  foreach (element_children($form['questions']) as $key) {
    $row = array();
    // Strip labels
    $form['questions'][$key]['enabled']['#title'] = '';
    $form['questions'][$key]['category']['#title'] = '';
    $form['questions'][$key]['question']['#title'] = '';
    $form['questions'][$key]['description']['#title'] = '';
    $form['questions'][$key]['multiple']['#title'] = '';
    $form['questions'][$key]['update']['#title'] = '';
    $form['questions'][$key]['delete']['#title'] = '';
        
    $row[] = drupal_render($form['questions'][$key]['enabled']);
    $row[] = drupal_render($form['questions'][$key]['category']);
    $row[] = drupal_render($form['questions'][$key]['question']);
    $row[] = drupal_render($form['questions'][$key]['description']);
    $row[] = drupal_render($form['questions'][$key]['multiple']);
    $row[] = drupal_render($form['questions'][$key]['update']);
    $row[] = drupal_render($form['questions'][$key]['delete']);
    $rows[] = $row;
  }

  $header = array('Enabled', '<a href="' . base_path() . 'admin/settings/surveys/' . $form['questions'][0]['node_type']['#value']  .'/categories">Edit Categories</a>', 'Questions', 'Description', 'Multiple Answers', 'Update / Save', 'Delete');

  // Header
  $form['questions']['#children'] = theme('table', $header, $rows);

  $output .= drupal_render($form);
  return $output;
}




