<?php

/**
 * @file
 * Surveys Select Questions UI
 *
 */

/**
 * Menu callback for /node/%/manage-survey/select.
 */
function surveys_select(&$form_state, $name) {

  $node = node_load(arg(1));
  global $user;
  
  drupal_set_title(t('Select Questions for %title', array('%title' => check_plain($node->title))));

  $form['questions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Available questions.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );
  
  // Get questions for this node type  
  $result = db_query("SELECT sq.qid, sq.question, sq.cid, sq.enabled, sq.node_type, sq.description, sc.category 
                      FROM {surveys_questions} sq 
                      LEFT JOIN {surveys_categories} sc ON sq.cid = sc.cid 
                      WHERE sq.enabled = 1 
                      AND sq.node_type = '%s' 
                      AND sq.qid NOT IN ( SELECT qid FROM {surveys_manage} sm WHERE nid = %d AND uid = %d)
                      ORDER BY category, question ASC", 
                      $node->type, $node->nid, $user->uid);

  $questions = array();
  while ($record = db_fetch_object($result)) {
    $record->nid = $node->nid;
    $record->uid = $node->uid;
    $questions[] = _surveys_select($record);
  }

  $form['questions'] += $questions;

  if ($user->uid == $node->uid) {
    $form['submit'] = array( '#type' => 'submit', '#value' => t('Add question/s'), '#disabled' => empty($questions) ? TRUE : FALSE, );
  }
  else {
    drupal_set_message(t('You are not the author of this content, therefore you cannot edit its survey content.'));
  }
  
  return $form;
  
}


/**
 * Select Form
 *
 */
function _surveys_select($record) {
  
  $form['qid'] = array(
    '#type' => 'hidden',
    '#value' => $record->qid,
  );
  $form['nid'] = array(
    '#type' => 'hidden',
    '#value' => $record->nid,
  );
  $form['uid'] = array(
    '#type' => 'hidden',
    '#value' => $record->uid,
  );
  $form['add'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#return_value' => 1,
    '#default_value' => 0,
    '#description' => t(''),
  );
  $form['question_display'] = array(
    '#type' => 'markup',
    '#value' => '<h3>' . $record->question . '</h3>',
  );  
  $form['question'] = array(
    '#type' => 'hidden',
    '#value' => $record->question,
  );  
  $form['category'] = array(
    '#type' => 'markup',
    '#title' => t('Category'),
    '#return_value' => 0,
    '#value' => '<h4>' . $record->category . '</h4>',
  );    
  $form['description'] = array(
    '#type' => 'markup',
    '#title' => t('description'),
    '#return_value' => 0,
    '#value' => '<p>' . $record->description . '</p>',
    '#rows' => 2,
  );

  return $form;
}

/**
 * Select Submit
 */
function surveys_select_submit($form, $form_state) {
  $form_values = $form_state['values'];

  foreach ($form_values['questions'] as $question) {
   if ($question['add']) {
      // Add a new question
      db_query("INSERT INTO {surveys_manage} (nid, uid, qid) VALUES (%d, %d, %d)", $question['nid'], $question['uid'], $question['qid']);
      drupal_set_message(t('The question (' . $question['question'] . ') has been added.'));
    }
    else {
      // Doesn't exist and don't use, so just ignore
    }
  }
}


/**
 * Implementation of theme_surveys_select()
 * - View in table format
 */
function theme_surveys_select($form) {
//dsm($form);
  $rows = array();
  $output = '';
  foreach (element_children($form['questions']) as $key) {
    $row = array();
    // Strip out labels 
    $form['questions'][$key]['add']['#title'] = '';
    $form['questions'][$key]['category']['#title'] = '';
    $form['questions'][$key]['question_display']['#title'] = '';
    $form['questions'][$key]['description']['#title'] = '';
        
    $row[] = drupal_render($form['questions'][$key]['add']);
    $row[] = drupal_render($form['questions'][$key]['category']);
    $row[] = drupal_render($form['questions'][$key]['question_display']);
    $row[] = drupal_render($form['questions'][$key]['description']);
    $rows[] = $row;
  }

  $header = array('Add', 'Categories', 'Questions', 'Description');

  // Header
  $form['questions']['#children'] = theme('table', $header, $rows);

  $output .= drupal_render($form);
  return $output;
}




